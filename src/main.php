<?php
/**
 * Created by IntelliJ IDEA.
 * User: kittiza
 * Date: 6/9/2018 AD
 * Time: 12:46
 */



class main
{
    public $db;
    public function __construct()
    {
        try {

            $conn = new PDO("mysql:host=kittiza.com;dbname=webapp;charset=utf8", 'root', 'zxc091321');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->db = $conn;
        } catch (PDOException $e) {
            print "Error! ####: " . $e->getMessage() . "<br/>";
            exit();
        }
    }
    public function query($sql = '', $prepare = [])
    {
        $result = $this->db->prepare($sql);
        $result->execute($prepare);
        return $result;
    }


    public function view($file,$string = []){
        foreach ($string as $k => $v){
            $$k = $v;
        }
        if (!file_exists("templates/view.$file.php")){
            $file = "error";
        }
        require_once "templates/view.header.php";
        require_once "templates/view.$file.php";
        require_once "templates/view.footer.php";
    }
}