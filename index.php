<?php
/**
 * Created by IntelliJ IDEA.
 * User: kittiza
 * Date: 6/9/2018 AD
 * Time: 12:30
 */


require __DIR__.'/src/main.php';
$app = new main();
$page = $_GET["page"];
if (!isset($page)){
    $page = "error";
}
$base_url = 'http://'.$_SERVER['HTTP_HOST'];
switch ($page){
    case "home":
        $category = $_GET['category'];
        $sql = (object) $app->query("select * from category;")->fetchAll();
        $m = (object) $app->query("select * from menu;")->fetchAll();
        if ($category != "" && $category != "ทั้งหมด"){
            $m = (object) $app->query("select * from menu  where `category` = ?;",[$category])->fetchAll();
        }
        $app->view("home",[
            "title" => "หน้าแรก",
            "sql" => $sql,
            "c" => $category,
            "m"=>$m,
            "page" => $page
        ]);
        break;

    case "view":
        $n = $_GET['name'];
        $category = $_GET['category'];

        $sql = (object) $app->query("select * from category;")->fetchAll();
        $m = (object) $app->query("select * from menu where `name` = ?;",[$n])->fetch();
        if ($m == null){

        }
        $c = $m->category;

        $app->view("view",[
            "title" => "เมนู - ".$n,
            "m"=>$m,
            "name" => $n,
            "page" => $page,
            "sql" => $sql,
            "c" => $c,
        ]);
        break;
    default:
        $category = "ทั้งหมด";
        $sql = (object) $app->query("select * from category;")->fetchAll();
        $m = (object) $app->query("select * from menu;")->fetchAll();
        $app->view("home",[
            "title" => "หน้าแรก",
            "sql" => $sql,
            "c" => $category,
            "m"=>$m,
            "page" => $page
        ]);
        break;
}


?>