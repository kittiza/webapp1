<?php
/**
 * Created by IntelliJ IDEA.
 * User: kittiza
 * Date: 6/9/2018 AD
 * Time: 12:53
 */
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/app.css">
    <title><?php echo $title?></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">อาหารไทย</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/"> หน้าแรก</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ประเภทอาหาร
                </a>
                <div class="dropdown-menu">
                    <?php
                    if ($c == "" || $c == "ทั้งหมด"){
                        echo '<a class="dropdown-item active" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                    }else{
                        echo '<a class="dropdown-item" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                    }
                    foreach ($sql as $v){
                        $n = $v['name'];
                        $h = "?page=$page&category=$n";
                        if ($c == $n){
                            echo '<a class="dropdown-item active" href="'.$h.'">'.$n.'</a>';
                        }else{
                            echo '<a class="dropdown-item" href="'.$h.'">'.$n.'</a>';
                        }
                    }
                    ?>

                </div>
            </li>
        </ul>
    </div>
</nav>
