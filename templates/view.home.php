<?php
/**
 * Created by IntelliJ IDEA.
 * User: kittiza
 * Date: 6/9/2018 AD
 * Time: 12:40
 */

?>

<div class="container top-sm">
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">ประเภทอาหาร</h5>
                    <div class="card-text">
                        <div class="nav flex-column nav-pills">
                            <?php
                            if ($c == "" || $c == "ทั้งหมด"){
                                echo '<a class="nav-link active" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                            }else{
                                echo '<a class="nav-link" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                            }
                            foreach ($sql as $v){
                                $a = "";
                                $h = "?page=home&category=".$v['name'];
                                $n = $v['name'];
                                if ($c == $n){
                                    $a = "active";
                                }
                                echo '<a class="nav-link '.$a.'" href="'.$h.'">'.$n.'</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">รายการอาหาร - <?php echo $c?></h5>
                    <div class="container">
                        <div class="row">

                            <?php
                            foreach ($m as $v){
                                $tit = $v['name'];
                                $img = $v['img'];
                                $tm = '<div class="col-md-4">
                                <div class="card text-center">
                                    <img class="card-img-top" src="'.$img.'">
                                    <div class="card-body">
                                        <h5 class="card-title">'.$tit.'</h5>
                                        <a href="?page=view&name='.$tit.'&category='.$v['category'].'" class="btn btn-primary">ดูข้อมูลเพิ่มเติม</a>
                                    </div>
                                    </div>
                                    </div>';
                                echo $tm;
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


