<?php
/**
 * Created by IntelliJ IDEA.
 * User: kittiza
 * Date: 7/9/2018 AD
 * Time: 19:35
 */
?>


<div class="container top-sm">
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">ประเภทอาหาร</h5>
                    <div class="card-text">
                        <div class="nav flex-column nav-pills">
                            <?php
                            if ($c == "" || $c == "ทั้งหมด"){
                                echo '<a class="nav-link active" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                            }else{
                                echo '<a class="nav-link" href="?page=home&category=ทั้งหมด">ทั้งหมด</a>';
                            }
                            foreach ($sql as $v){
                                $a = "";
                                $h = "?page=home&category=".$v['name'];
                                $n = $v['name'];
                                if ($c == $n){
                                    $a = "active";
                                }
                                echo '<a class="nav-link '.$a.'" href="'.$h.'">'.$n.'</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-9">
            <div class="card">
                <img class="card-img-top" height="400px" src="<?php echo $m->img?>"  alt="">
                <div class="card-body">
                    <h3 class="card-title"><?php echo $name?></h3>
                    <p class="card-text">ประเภท : <?php echo $c?></p>
                    <p class="card-text">รายละเอียด : </p>
                    <?php echo $m->info?>
                    <br>
                    <span class="text-warning">★ ★ ★ ★ ☆</span>
                    4.0 ดาว
                    <br>
                    <p class="card-text">วัตถุดิบ</p>
                    <?php echo $m->material?>
                    <p class="card-text">วิธีทำ</p>
                    <?php echo $m->howto?>
                </div>
            </div>

        </div>

    </div>
</div>


